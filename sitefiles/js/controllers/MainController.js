app.controller('MainController', ['$scope', function($scope) { 
  
   $scope.images = [
      { 
        title: 'hondentrimster', 
        src: 'sitefiles/images/foto/hondentrimster.jpg', 
           number: 1,
      },
      { 
        title: 'loyale bewoonster', 
        src: 'sitefiles/images/foto/loyale_bewoonster.jpg', 
         number: 2,
      },
      { 
        title: 'ambtenaren', 
        src: 'sitefiles/images/foto/ambtenaren.jpg', 
         number: 3,
      },
      { 
        title: 'dierenvriend', 
        src: 'sitefiles/images/foto/dierenvriend.jpg', 
         number: 4,
      },
      { 
        title: 'happy family', 
        src: 'sitefiles/images/foto/happy_family.jpg', 
         number: 5,
      },
      { 
        title: 'techneut', 
        src: 'sitefiles/images/foto/techneut.jpg', 
         number: 6,
      },
      { 
        title: 'fietser', 
        src: 'sitefiles/images/foto/fietser.jpg', 
         number: 7,
      },
      { 
        title: 'duits echtpaar', 
        src: 'sitefiles/images/foto/duits_echtpaar.jpg', 
         number: 8,
      },
      { 
        title: 'fijnproevers', 
        src: 'sitefiles/images/foto/fijnproevers.jpg', 
         number: 9,
      },
      { 
        title: 'edelsmid', 
        src: 'sitefiles/images/foto/edelsmid.jpg', 
         number: 10,
      },
      { 
        title: 'accountant', 
        src: 'sitefiles/images/foto/accountant.jpg', 
         number: 11,
      }, 
      { 
        title: 'gewortelde man', 
        src: 'sitefiles/images/foto/gewortelde_man.jpg', 
         number: 12,
      },
      { 
        title: 'straatvoetballer', 
        src: 'sitefiles/images/foto/voetballer.jpg', 
         number: 13,
      },
      { 
        title: 'oud bewoners', 
        src: 'sitefiles/images/foto/oud-bewoners.jpg', 
         number: 14,
      },
      { 
        title: 'moeder en zoon', 
        src: 'sitefiles/images/foto/moeder_en_zoon.jpg', 
         number: 15,
      }, 
      { 
        title: 'ruiter', 
        src: 'sitefiles/images/foto/ruiter.jpg', 
           number: 16,
      }, 
      { 
        title: 'ex-mijnwerker', 
        src: 'sitefiles/images/foto/ex-mijnwerker.jpg', 
         number: 17,
      }, 
      { 
        title: 'kweker', 
        src: 'sitefiles/images/foto/kweker.jpg', 
         number: 18,
      }, 
       
      

	]
  
    $scope.quadrants = [
      { 
         ref: '1-1', 
         src: 'sitefiles/images/map/map1-1.jpg',
         markers: [
            {
               number: 40,
               positionTop:'68',
               positionLeft:'29',
               title:'De kerkklok, en verder',
               text1:'Over het uitzicht vanaf de flats raakt men niet uitgesproken: hoe hoger het appartement zich bevond, hoe meer waarde het om die reden leek te vertegenwoordigen. Niet alleen om de directe omgeving te bekijken, of verder weg, richting Aken, Monschau of zelfs naar België, maar ook om te weten hoe laat het was: voor wie in de flat woonde, was het niet zelden de kerkklok waarnaar in een reflex gekeken werd. En éénmaal per jaar was er een heuse samenkomst op de galerijen om het vuurwerk van de verre omgeving te aanschouwen. De galerijen waren dan ook voor iedereen toegankelijk in de beginjaren, dat maakte dat veel omwonenden wel eens een kijkje gingen nemen.',
            },
            {
               number: 11,
               positionTop:'37',
               positionLeft:'74',
               title:'De elementen',
               text1:'Voordat de flats er stonden was het hier heel anders. De wind en de kou hadden vrij spel, vooral de bewoners van de Ursulastraat hadden hiermee te maken. Dit is volledig veranderd, ten goede welteverstaan. De flats vormen een windscherm welke het wonen hier een stukje aangenamer maakt. Een buffer, zoals het vaak genoemd wordt. Een enkeling meent dat er door de flats ook sprake is van valwind, maar dit wordt bevestigd noch ervaren door anderen.',
            },
         ]
      }, 
       { 
         ref: '1-2', 
         src: 'sitefiles/images/map/map1-2.jpg',
         markers: [
            {
               number: 2,
               positionTop:'90',
               positionLeft:'86',
               title:'De fietser',
               text1:'Een herkenning voor de eenzame fietser die vanuit Horbach de laatste strohalm zoekt en zich nog één keer op de pedalen zet om binnen de tijd de eindstreep te bereiken. In heel de regio zijn ze gebouwd, of misschien zelfs in heel Nederland. Eind jaren ’60 ongeveer. Hier in de omgeving zijn er een aantal plekken waar ze te vinden zijn, maar die van Bleijerheide zijn voor de fietser het meest herkenbaar: wanneer ze aan de horizon opdoemen, weet hij dat hij bijna thuis is.',
            },
            {
               number: 11,
               positionTop:'57',
               positionLeft:'38',
               title:'De elementen',
               text1:'Voordat de flats er stonden was het hier heel anders. De wind en de kou hadden vrij spel, vooral de bewoners van de Ursulastraat hadden hiermee te maken. Dit is volledig veranderd, ten goede welteverstaan. De flats vormen een windscherm welke het wonen hier een stukje aangenamer maakt. Een buffer, zoals het vaak genoemd wordt. Een enkeling meent dat er door de flats ook sprake is van valwind, maar dit wordt bevestigd noch ervaren door anderen.',
            },
            {
               number: 33,
               positionTop:'91',
               positionLeft:'31',
               title:'Wereldburgers',
               text1:'De bellentableaus verraden het al, de ex-bewoners kunnen er geanimeerd over vertellen. Niet alleen negatieve verhalen over ruzies in onverstaanbare volzinnen en naar buiten gegooid afval, ook positief: er werd samen gespeeld door kinderen van verschillende culturen, allemaal aanwezig in de flat. Het begon met Amerikanen en Canadezen, later kwamen er Bosniërs, Polen, Roemenen, Bulgaren en Afrikanen. Ieder met een stukje eigen cultuur, niet zelden zichtbaar van buiten, maar wel hoorbaar: de muziekkeuze verraadde al heel wat. Maar op het voetbalveld was iedereen gelijk en telde maar één ding: de winst.',
            },
         ]
      },
       { 
         ref: '1-3', 
         src: 'sitefiles/images/map/map1-3.jpg',
         markers: [
            {
               number: 11,
               positionTop:'27',
               positionLeft:'16',
               title:'De elementen',
               text1:'Voordat de flats er stonden was het hier heel anders. De wind en de kou hadden vrij spel, vooral de bewoners van de Ursulastraat hadden hiermee te maken. Dit is volledig veranderd, ten goede welteverstaan. De flats vormen een windscherm welke het wonen hier een stukje aangenamer maakt. Een buffer, zoals het vaak genoemd wordt. Een enkeling meent dat er door de flats ook sprake is van valwind, maar dit wordt bevestigd noch ervaren door anderen.',
            },
            {
               number: 11,
               positionTop:'71',
               positionLeft:'45',
               title:'De elementen',
               text1:'Voordat de flats er stonden was het hier heel anders. De wind en de kou hadden vrij spel, vooral de bewoners van de Ursulastraat hadden hiermee te maken. Dit is volledig veranderd, ten goede welteverstaan. De flats vormen een windscherm welke het wonen hier een stukje aangenamer maakt. Een buffer, zoals het vaak genoemd wordt. Een enkeling meent dat er door de flats ook sprake is van valwind, maar dit wordt bevestigd noch ervaren door anderen.',
            },
            {
               number: 1,
               positionTop:'86',
               positionLeft:'25',
               title:'De Ster',
               text1:'De ster was er altijd. Ieder jaar opnieuw, van ver zichtbaar, een baken in de duisternis in de ware zin van het woord. Daar was thuis. En thuis was een weids begrip tijdens de feestdagen, de ster straalde immers over de flats heen: naar Heerlen, naar Duitsland. De regio als een wollige golvende deken waar op de toppen en in de dalen geen enkel plekje geheimen kent, waar blindelings de weg gevonden wordt, waar familienamen zacht klinken en het Duits een gemeenschappelijke grond biedt.',
               text2:'Zoals het een ster betaamt, tovert deze een magische glimlach op het gezicht van een ieder die eraan herinnerd wordt. De ster, ja, dat was mooi. Ieder jaar opnieuw. De ster was er voor iedereen: flatbewoner, buurtbewoner, Euregiaan.',
            },
         ]
      }, 
       { 
         ref: '2-1', 
         src: 'sitefiles/images/map/map2-1.jpg',
         markers: [
            {
               number: 40,
               positionTop:'26',
               positionLeft:'52',
               title:'De kerkklok, en verder',
               text1:'Over het uitzicht vanaf de flats raakt men niet uitgesproken: hoe hoger het appartement zich bevond, hoe meer waarde het om die reden leek te vertegenwoordigen. Niet alleen om de directe omgeving te bekijken, of verder weg, richting Aken, Monschau of zelfs naar België, maar ook om te weten hoe laat het was: voor wie in de flat woonde, was het niet zelden de kerkklok waarnaar in een reflex gekeken werd. En éénmaal per jaar was er een heuse samenkomst op de galerijen om het vuurwerk van de verre omgeving te aanschouwen. De galerijen waren dan ook voor iedereen toegankelijk in de beginjaren, dat maakte dat veel omwonenden wel eens een kijkje gingen nemen.',
            },
            {
               number: 8,
               positionTop:'70',
               positionLeft:'40',
               title:'Meer licht in huis',
               text1:'Hoog zijn ze, de flats. Niet alleen horizonbepalend maar ook van invloed op het visuele microklimaat in de wijk. Rondom bewegend nemen ze allerlei gedaantes aan: muur, toren, blok, steeds meer of minder aanwezig, mede door het geaccidenteerde terrein: vanuit iedere hoek een ander perspectief. De lange zijden - de balkons en galerijen – zijn al een aantal keren vernieuwd of voorzien van een andere kleur. Een terughoudende kleur wekte de minste weerstand, misschien ook omdat de uitbundige kleuren waar een jaar of 30 geleden voor gekozen is op weinig sympathie konden rekenen, voorzichtig uitgedrukt.',
               text2:'De zijgevels hebben ook al meerdere gedaantes aangenomen, al zijn de kleuren hier minder uitgesproken geweest. Toch hebben veranderingen hier ook invloed op de omgeving: Toen de zijgevels van de C-flat opnieuw geschilderd werden, dit maal een aantal tonen lichter, klaarde het op in een groot aantal huizen eromheen, alsof de zonwering omhoog werd getrokken en het licht weer naar binnen kon stromen. Alsof de flat alvast een stapje terug deed, een voorschot nemend op de toekomst.',
            },
            {
               number: 10,
               positionTop:'85',
               positionLeft:'62',
               title:'Fundering',
               text1:'Dat de flat bouwtechnisch gezien zijn beste tijd achter de rug heeft, dat is geen geheim. De gestutte balkons zijn daar een duidelijk voorbeeld van, en het kraken en bewegen van de flats is ook meerdere malen ter sprake gekomen. Wilma, een aannemersbedrijf uit Weert dat bestond van 1939 tot 1998, heeft ze indertijd gebouwd, niet alleen hier, maar zowat in heel Nederland. Bij menigeen staan ze dan ook bekend als ‘de Wilma-flats’. Niet alleen de balkons schijnen een probleem te zijn, ook de fundering. Niemand weet exact hoe het zit maar er wordt gezegd dat deze bij lange na niet diep genoeg is, dat schijnt te maken te hebben met mijngangen en de daarmee samenvallende onmogelijkheid om in de diepere aardlagen steun te vinden. Of dit ook de voornaamste reden voor sloop is, dat lijkt nog bij niemand bekend. Waarschijnlijk zal het ook en vooral te maken hebben met een veranderende woonbehoefte en afschrijving.',
            },
            {
               number: 17,
               positionTop:'82',
               positionLeft:'74',
               title:'Voetballen',
               text1:'Er werd hier al gevoetbald voordat de flats er stonden. BVV Sparta, opgericht in 1914, speelde hier zijn wedstrijden en ligt hiermee als één van de fusieclubs waaruit Roda JC is ontstaan aan de basis van deze club. Sinds de bouw ligt het officiële voetbalveld van de wijk, bespeeld door KVC Oranje, ernaast maar werden de wedstrijden die er écht toe doen tussen de flats gespeeld. Jongeren uit de flats en uit de buurt samen, zonder enig onderscheid te maken in geboorteplaats, woonplaats of land van herkomst. Het bankje bij het voetbalveld staat in het geheugen van vele (inmiddels) volwassenen gegrift, op deze plek kwam je altijd wel iemand tegen, was het niet om daadwerkelijk tegen een bal te trappen, dan toch om een beetje rond te hangen. Ouders vonden het wel prima dat daar gespeeld werd want het was een veilig, overzichtelijk en goed omheind terrein.',
            },
            {
               number: 9,
               positionTop:'77',
               positionLeft:'92',
               title:'Gemeenschap',
               text1:'In de flat, tussen de haakjes, was er altijd wel iemand. Niet dat men iedereen kende, maar hoe laat (of vroeg)  het ook was, er was altijd wel een goedendag in de hal, in de lift of op de galerij. Ieder leefde wel zijn eigen leven, maar samen vormden ze een wereld op zich, een gemeenschap die terughoudend aanwezig was voor degene die het nodig had. Banden met de buitenwereld waren er ook meer dan voldoende, het dagelijks leven maakte geen onderscheid tussen flat- of huisbewoner. Gaandeweg is dit gevoel afgekalfd, de bewoners van weleer vertrokken en de nieuwelingen gedroegen zich alsof er geen ander bestond.',
            },
         ]
      },
       { 
         ref: '2-2', 
         src: 'sitefiles/images/map/map2-2.jpg',
         markers: [
            
            {
               number: 2,
               positionTop:'3.5',
               positionLeft:'85.5',
               title:'De fietser',
               text1:'Een herkenning voor de eenzame fietser die vanuit Horbach de laatste strohalm zoekt en zich nog één keer op de pedalen zet om binnen de tijd de eindstreep te bereiken. In heel de regio zijn ze gebouwd, of misschien zelfs in heel Nederland. Eind jaren ’60 ongeveer. Hier in de omgeving zijn er een aantal plekken waar ze te vinden zijn, maar die van Bleijerheide zijn voor de fietser het meest herkenbaar: wanneer ze aan de horizon opdoemen, weet hij dat hij bijna thuis is.',
            },
            {
               number: 3,
               positionTop:'30',
               positionLeft:'72',
               title:'Aardappelen',
               text1:'De aardappel houdt verband met Bleijerheide in meerdere opzichten. Ten eerste is er het belang van de ondergrond voor het leven erboven. De schacht van de kolenmijn Neuprick daalde af in het hart van de huidige wijk en doorzeefde de aarde tot een diepte van 235 meter, bij lange na niet zo ingrijpend als de Domaniale maar evengoed van belang voor het bovengrondse leven. Er wordt gezegd dat de flats gebouwd zijn op een zeer beperkte fundering, dieper graven had geen zin want op een gatenkaas kun je immers niet bouwen.',
               text2:'Zoals een aardappel als onderaardse knol groeit om uiteindelijk gerooid te worden, tot voedsel te dienen en energie te leveren, zo diende de steenkool in de Bleijerheidse grond hetzelfde doel.  Echter, waar een aardappel in te passen is in een ecosysteem en er met behoedzame omgang met grondstoffen en energie een onuitputtelijke bodem kan blijven bestaan, zo is de kolenwinning per definitie een eindig leven beschoren. En zodoende bleven terreinen achter, verdwenen generaties en kwamen er nieuwe, om te beantwoorden aan de vraag naar arbeidskrachten op de tot industrie- en bedrijvenzones herbestemde mijnterreinen. Deze generatie zocht – en vond - huisvesting in en om de flats. De flats, die vanaf het begin waren als een vakkundig gemaakte mand vol vers gerooide aardappelen: van alle gemakken voorzien, veel licht, lucht en ruimte en met goede buren. En zoals een mand vol aardappelen een zekere mate van onderhoud nodig heeft, verdient ook de nieuwe micro-maatschappij in Bleijerheide een zekere toewijding. Want rotte knollen steken ook de rest aan: het tijdig verwijderen hiervan voorkomt besmetting. Dit heeft volgens velen het uiteindelijk lot van de flats bepaald: de rotte knollen zijn niet tijdig gesignaleerd, of sterker nog: zouden doelbewust in de mand zijn gelegd, al dan niet vanuit de ijdele hoop dat hiermee het rottingsproces een halt zou worden toegeroepen.',
            },
            
            {
               number: 4,
               positionTop:'76',
               positionLeft:'54',
               title:'Het bellentableau',
               text1:'De bellentableaus bevatten een schat aan informatie voor wie goed kijkt. Een herinnering voor de bloemist die zijn kunststukjes tot aan de deur brengt, voor de jonge voetballer die vroeger blindelings zijn weg vond tot aan zijn maatjes en nu de mentale kaart van de flat opnieuw kan tekenen door de namen op te lezen, voor de pessimist die op basis van exotisch en allochtoon klinkende namen een bevestiging vindt in zijn constatering dat het op sociaal gebied bergafwaarts is gegaan, voor de optimist die hier een omgekeerd evenredige conclusie uit trekt en een multicultureel samenleven op basis van wederzijds respect aantreft. Met de nummers komen de namen en met de namen komen de gezichten van de lang of minder lang geleden vertrokken bewoners.',
            },
            {
               number: 5,
               positionTop:'17',
               positionLeft:'10',
               title:'Van licht naar leegte',
               text1:'De lichtjes van de galerij, woon- en slaapkamers waren er altijd. Ze toonden het leven in – en het ritme van - de flat. Wie dit niet kon of wilde zien, zag vooral een onmenselijke machine, toonbeeld van verleden tijd en hèt voorbeeld van hoe het niet moet, een onvoorstelbaar leven in de schaduw van zo’n monstrueus blok. Voor de aandachtige beschouwer daarentegen vormde het een bescherming tegen de elementen, een veilige muur die veel meer is dan een muur: een lichtkrant bijna, die steeds een ander verhaal vertelt en een blik gunt in de wereld aan de overkant. Achter ieder lichtje een verhaal, een moment, een aanwezigheid. Een aanwezigheid van bijna 50 jaar, en dat maal vier. De ene zijde – de galerij - was er altijd en sliep nooit, de andere toonde het leven zoals het was. En zelfs hier was er altijd wel een lichtje, hoe donker of laat het ook was. Dat leven is nu zo goed als verdwenen, het baken is nu zelf schaduw geworden. Plots is de hoogte geen zegen meer maar een bedreiging, is ieder lichtje dat definitief dooft een verhaal minder, een uitgelezen boek. Het verlies laat zich het hardst voelen in de schaduw.',
            },
            {
               number: 5,
               positionTop:'10',
               positionLeft:'67',
               title:'Van licht naar leegte',
               text1:'De lichtjes van de galerij, woon- en slaapkamers waren er altijd. Ze toonden het leven in – en het ritme van - de flat. Wie dit niet kon of wilde zien, zag vooral een onmenselijke machine, toonbeeld van verleden tijd en hèt voorbeeld van hoe het niet moet, een onvoorstelbaar leven in de schaduw van zo’n monstrueus blok. Voor de aandachtige beschouwer daarentegen vormde het een bescherming tegen de elementen, een veilige muur die veel meer is dan een muur: een lichtkrant bijna, die steeds een ander verhaal vertelt en een blik gunt in de wereld aan de overkant. Achter ieder lichtje een verhaal, een moment, een aanwezigheid. Een aanwezigheid van bijna 50 jaar, en dat maal vier. De ene zijde – de galerij - was er altijd en sliep nooit, de andere toonde het leven zoals het was. En zelfs hier was er altijd wel een lichtje, hoe donker of laat het ook was. Dat leven is nu zo goed als verdwenen, het baken is nu zelf schaduw geworden. Plots is de hoogte geen zegen meer maar een bedreiging, is ieder lichtje dat definitief dooft een verhaal minder, een uitgelezen boek. Het verlies laat zich het hardst voelen in de schaduw.',
            },
            {
               number: 6,
               positionTop:'39',
               positionLeft:'20',
               title:'Ogen op straat',
               text1:'Vier maal honderd paar ogen. Ogen die signaleerden, bijvoorbeeld die keer dat het bezoek een handtas achterliet na het op slot zetten van de fiets en dit binnen enkele minuten via een telefoontje gemeld werd. Of de emmer die naast de auto bleef staan na het wassen, en waarop de autowasser binnen de kortste keren gewezen werd. Maar ook andersom: een oudere mevrouw die om hulp riep vanaf haar balkon en binnen de kortste keren de gevraagde hulp aangeboden kreeg. Jane Jacobs,  Amerikaans-Canadese publiciste en stadsactiviste, vooral bekend geworden door haar boek ‘The Death and Life of Great American Cities’, noemde dit verschijnsel ‘ogen op straat’. Ogen waarvan men wist dat er altijd wel een paar aanwezig was, die in de meeste gevallen een natuurlijk vertrouwd gevoel gaven en soms een gevoel van beklemming opriepen, aangezien ze deden beseffen dat hier niets ongezien kan gebeuren. Deze ogen - eerst Bleijerheids, later Kerkraads, Canadees, Amerikaans, Hollands, Bosnisch en uit landen waar de bewoners van het eerste uur het bestaan nauwelijks afwisten – deze ogen doofden langzaam uit. Allereerst richtten ze zich meer inwaarts; een gevolg van maatschappelijke en sociale veranderingen, zegt men. Daarna was er de onherroepelijke leegloop, als gevolg van de grote transformatie-opgave. Vanaf dit moment verloren de flats hun betekenis en meerwaarde en gingen ze meer vragen van de buurt dan ze konden geven.',
            },
            {
               number: 9,
               positionTop:'77',
               positionLeft:'02',
               title:'Gemeenschap',
               text1:'In de flat, tussen de haakjes, was er altijd wel iemand. Niet dat men iedereen kende, maar hoe laat (of vroeg)  het ook was, er was altijd wel een goedendag in de hal, in de lift of op de galerij. Ieder leefde wel zijn eigen leven, maar samen vormden ze een wereld op zich, een gemeenschap die terughoudend aanwezig was voor degene die het nodig had. Banden met de buitenwereld waren er ook meer dan voldoende, het dagelijks leven maakte geen onderscheid tussen flat- of huisbewoner. Gaandeweg is dit gevoel afgekalfd, de bewoners van weleer vertrokken en de nieuwelingen gedroegen zich alsof er geen ander bestond.',
            },
            {
               number: 10,
               positionTop:'86',
               positionLeft:'4',
               title:'Fundering',
               text1:'Dat de flat bouwtechnisch gezien zijn beste tijd achter de rug heeft, dat is geen geheim. De gestutte balkons zijn daar een duidelijk voorbeeld van, en het kraken en bewegen van de flats is ook meerdere malen ter sprake gekomen. Wilma, een aannemersbedrijf uit Weert dat bestond van 1939 tot 1998, heeft ze indertijd gebouwd, niet alleen hier, maar zowat in heel Nederland. Bij menigeen staan ze dan ook bekend als ‘de Wilma-flats’. Niet alleen de balkons schijnen een probleem te zijn, ook de fundering. Niemand weet exact hoe het zit maar er wordt gezegd dat deze bij lange na niet diep genoeg is, dat schijnt te maken te hebben met mijngangen en de daarmee samenvallende onmogelijkheid om in de diepere aardlagen steun te vinden. Of dit ook de voornaamste reden voor sloop is, dat lijkt nog bij niemand bekend. Waarschijnlijk zal het ook en vooral te maken hebben met een veranderende woonbehoefte en afschrijving.',
            },
            
            {
               number: 10,
               positionTop:'86',
               positionLeft:'62',
               title:'Fundering',
               text1:'Dat de flat bouwtechnisch gezien zijn beste tijd achter de rug heeft, dat is geen geheim. De gestutte balkons zijn daar een duidelijk voorbeeld van, en het kraken en bewegen van de flats is ook meerdere malen ter sprake gekomen. Wilma, een aannemersbedrijf uit Weert dat bestond van 1939 tot 1998, heeft ze indertijd gebouwd, niet alleen hier, maar zowat in heel Nederland. Bij menigeen staan ze dan ook bekend als ‘de Wilma-flats’. Niet alleen de balkons schijnen een probleem te zijn, ook de fundering. Niemand weet exact hoe het zit maar er wordt gezegd dat deze bij lange na niet diep genoeg is, dat schijnt te maken te hebben met mijngangen en de daarmee samenvallende onmogelijkheid om in de diepere aardlagen steun te vinden. Of dit ook de voornaamste reden voor sloop is, dat lijkt nog bij niemand bekend. Waarschijnlijk zal het ook en vooral te maken hebben met een veranderende woonbehoefte en afschrijving.',
            },
            {
               number: 12,
               positionTop:'57',
               positionLeft:'46',
               title:'De was',
               text1:'Een waar schouwspel was het: de was. Kraakwit of fel gekleurd, de geur van waspoeder en –verzachter was niet zelden denkbeeldig. Ze toonde de bedrijvigheid in de flat en verkleinde de schaal, van anoniem blok tot een thuis voor bewoners die ofwel graag toonden hoe bedreven ze waren in huishoudelijk werk, ofwel om pragmatische redenen de was graag spoedig en zonder kosten lieten drogen, en daar waren de hoge flats natuurlijk uitermate geschikt voor.',
            },
            {
               number: 13,
               positionTop:'78',
               positionLeft:'29',
               title:'100 nieuwe huizen',
               text1:'Dat is wat er schijnt te komen in de plaats van de flats. Hoe, in welke vorm, waar en wanneer is nog minder duidelijk. Over het algemeen is men het over eens dat het wel iets lager mag, die nieuwe bebouwing. Drie, vier of vijf lagen maximaal is meer dan genoeg. Misschien kan de onderste helft van de flats nog gebruikt worden? Dat is ecologisch en economisch gezien ook meer hedendaags dan botweg slopen en nieuw bouwen. Of misschien zelfs de onderste woningen verwijderen en bovenin nieuwe lofts maken en dus het geweldig uitzicht behouden.',
               text2:'Ja, de gemeente heeft ooit beloofd dat er hier een park zou komen. Er zijn mensen die daarom hier een huis hebben gekocht. Dat zou dan ook geweldig zijn, maar dat zit er vast niet meer in. Hier heerst dan ook wat onvrede over. Als het maar een beetje groen blijft, zoals nu. Hoe de nieuwe bebouwing er uit zal zien zal het probleem niet zijn. De grootste angst zijn de toekomstige bewoners: want stel je voor, er komen slechts mensen die elders niet terecht kunnen en weinig te besteden hebben, om nog maar niet te spreken over ‘buitenlanders’… Regelmatig wordt geopperd dat dat de doodsteek voor de buurt kan betekenen. ‘Ze’ hebben de laatste jaren al van alles in de flats gestopt, en je zag het achteruit gaan. Er werd gedeald op straat en er was regelmatig ruzie. Een goede mix in afkomst, koopkracht en misschien wel huur- en koopaanbod wordt alleszins toegejuicht.',
            },
            {
               number: 14,
               positionTop:'81',
               positionLeft:'10',
               title:'Rolluiken',
               text1:'In Limburg zie je ze regelmatig, in tegenstelling tot in de rest van Nederland. ’s Avonds, wanneer het donker is en mensen thuiskomen van hun werk, wordt het straatbeeld niet zelden bepaald door eentonige rijen woningen met hermetisch gesloten ramen en deuren. De bewoners mogen dan wel vriendelijke mensen zijn, maar zo’n straatbeeld wekt weinig sympathie en vertrouwen. Dit staat in schril contrast met de flat: hier was altijd wel een teken van leven, al was het maar door een enkel lampje dat nog ergens brandde.',
            },
            {
               number: 15,
               positionTop:'61',
               positionLeft:'44',
               title:'Bezorgdienst',
               text1:'De winkeliers en handelaren rondom de flats voeren er wel bij. Klanten kwamen net zo goed uit de flats als uit de andere huizen. En iets afleveren in de flats gebeurde ook regelmatig: boeketten en allerhande zuivelproducten werden tot aan de voordeur bezorgd, als het moest zelfs over de drempel. De galerijen waren zo geen onbekend terrein voor de buurtbewoners, met name de winkeliers en hun personeel. Zo kreeg het zicht op de flats een persoonlijk tintje, en waren de galerijen iets meer publiek dan de laatste jaren het geval was.',
            },
            {
               number: 30,
               positionTop:'29',
               positionLeft:'96',
               title:'Papier prikken',
               text1:'Vroeger had iedere flat een eigen huismeester, dit was dan vaak een man uit één van de gezinnen die de verantwoordelijkheid nam of kreeg voor de flat en de ruimte eromheen. Nu is dat anders: vanuit HEEMwonen is er een huismeester benoemd voor het hele terrein. Het is geweldig hoe die man het nog in de gaten houdt. Hij loopt bijvoorbeeld regelmatig met een papierprikker om de flats heen en ruimt al het zwerfvuil op. Ook is hij nooit te beroerd bewoners aan te spreken op zwerfvuil of andere overtredingen. Hij ziet in zijn eentje toe op de omgeving en dat is goud waard, zeker sinds de leegstand.',
            },
            {
               number: 32,
               positionTop:'20',
               positionLeft:'70',
               title:'Galerijlicht',
               text1:'De galerijen waren altijd aanwezig, al was het maar door het licht dat de hele nacht brandde. Het toonbeeld van regelmaat en netheid, aangezien er niets op mocht staan, geen was mocht hangen en er dus weinig tot niets uit de toon kon vallen en slechts het ritme van ramen, deuren, balustrades en lichtjes overbleef. De flats zijn nu leeg, maar de galerijlichtjes blijven volharden in een poging te doen geloven dat alles nog even kalm, vredig, geordend en overzichtelijk is als in de jaren ’60.',
            },
            {
               number: 33,
               positionTop:'4',
               positionLeft:'31',
               title:'Wereldburgers',
               text1:'De bellentableaus verraden het al, de ex-bewoners kunnen er geanimeerd over vertellen. Niet alleen negatieve verhalen over ruzies in onverstaanbare volzinnen en naar buiten gegooid afval, ook positief: er werd samen gespeeld door kinderen van verschillende culturen, allemaal aanwezig in de flat. Het begon met Amerikanen en Canadezen, later kwamen er Bosniërs, Polen, Roemenen, Bulgaren en Afrikanen. Ieder met een stukje eigen cultuur, niet zelden zichtbaar van buiten, maar wel hoorbaar: de muziekkeuze verraadde al heel wat. Maar op het voetbalveld was iedereen gelijk en telde maar één ding: de winst.',
            },
            {
               number: 36,
               positionTop:'78',
               positionLeft:'79',
               title:'Kraken',
               text1:'De flat ademt, letterlijk. Bij temperatuurverschillen hoor je ‘m af en toe kraken, knallen bijna. Het klinkt niet angstaanjagend, eerder vertrouwd. Bewegen deed hij sowieso al, waarschijnlijk om die reden zijn er een tijd geleden muurtjes gemetseld in de kelders, om de stijfheid te verhogen. Het geluid is er in ieder geval niet minder om geworden. Een levende reus die waakt over de wijk.',
            },
            
            {
               number: 37,
               positionTop:'41',
               positionLeft:'41',
               title:'Geluid',
               text1:'Radio, televisie, conversatie, ruzie, altijd was er wel een geluid te bespeuren en was de flat ook onzichtbaar aanwezig in de wijk. De aard van het geluid vertelde iets over de herkomst van de mensen die er verantwoordelijk voor waren: de soort muziek, de intensiteit van discussies, de taal natuurlijk, zo merkte men ook dat er steeds minder Kerkraadse en steeds meer bewoners van elders in de wereld in de flat huisden.',
            },
            {
               number: 37,
               positionTop:'66',
               positionLeft:'12',
               title:'Geluid',
               text1:'Radio, televisie, conversatie, ruzie, altijd was er wel een geluid te bespeuren en was de flat ook onzichtbaar aanwezig in de wijk. De aard van het geluid vertelde iets over de herkomst van de mensen die er verantwoordelijk voor waren: de soort muziek, de intensiteit van discussies, de taal natuurlijk, zo merkte men ook dat er steeds minder Kerkraadse en steeds meer bewoners van elders in de wereld in de flat huisden.',
            },
            {
               number: 41,
               positionTop:'28',
               positionLeft:'16',
               title:'Bioscoopzit',
               text1:'De balkons van de flats hadden een uitgekiende maat: het drogen van de was ging er prima. Met een beetje inventiviteit kon je er ook vrij prettig op zitten: niet diep genoeg om tegenover elkaar plaats te nemen, maar naast elkaar ging prima. Een beetje zoals in de bioscoop met de wijk als film. De balkons van de A-flat werden regelmatig zo gebruikt, het perfecte zicht op het voetbalveld maakte deze tot een ideale tribune. Maar ook de andere flats boden zo een goed zicht op de wijk en de verdere omgeving.',
            },
            {
               number: 42,
               positionTop:'61',
               positionLeft:'34',
               title:'Versiering',
               text1:'De ster die gedurende de kersperiode op één van de flats stond, was er voor iedereen. Op de balkons hingen mensen vaak eigen kerstversiering, zichtbaar vanuit de wijk, en was het niet op het balkon dan was het wel binnen, achter het glas maar evengoed zichtbaar voor de buitenwereld. Een kleurrijk schouwspel was het, dat de wijk eromheen in een warm licht zette. En was het geen kerst, dan was er altijd wel ergens de zonwering naar beneden. Zeker in de zomer was dit een kleurrijk gezicht: het mooist waren de markiezen, die zag je best veel op de flats.Woonwagen',
            },
            {
               number: 46,
               positionTop:'22',
               positionLeft:'28',
               title:'AFCENT',
               text1:'De B-flat, oftewel de tweede flat, heeft een ietwat afwijkend verhaal. De eerste bewoners die hier introkken na oplevering waren Amerikanen en Canadezen werkzaam op de legerbasis in Brunssum. Ze werden met busjes opgehaald en thuis gebracht, hadden weinig contact met de omwonenden (al leerden enkele middenstanders wel Engels om deze klanten beter te woord te kunnen staan in de winkel) maar gedroegen zich altijd keurig. Af en toe gaven ze feestjes, en er zullen vast wel eens wat drugs gebruikt zijn, maar problemen veroorzaakten ze eigenlijk nooit. Die begonnen pas later, toen de ‘Engelsen’ al lang vertrokken waren.',
            },
            {
               number: 47,
               positionTop:'80',
               positionLeft:'62',
               title:'A, B, C… 1, 2, 3',
               text1:'Wat de naamgeving van de flats betreft: ‘een veelheid aan’ doet wellicht meer recht aan de werkelijkheid dan ‘een eenduidigheid in’. De ‘Wilma-flats’ kwam een enkele keer ter sprake. De A-flat, B-flat, C-flat en D-flat waren veel gehoord, maar ook de eerste flat, de tweede flat, de derde flat en de vierde flat. Interessant werd het wanneer de cijfers niet meer overeen kwamen met de alfabetische volgorde: niet zelden werd er gesproken van bijvoorbeeld ‘de eerste flat’ wanneer het ging om de D-flat. Hier bleek een verklaarbare geografische reden voor te zijn: afhankelijk van de plek waar je je bevond, of vooral de plek waar de betreffende persoon woonde, was de eerste flat degene die het dichtst bij hem of haar stond, de tweede eentje verder, enzovoorts. Dat dit per straat of zelfs per huishouden anders was moge duidelijk zijn. Je zou er een lied over kunnen schrijven.',
            },
         ]
      },
       { 
         ref: '2-3', 
         src: 'sitefiles/images/map/map2-3.jpg',
         markers: [
            {
               number: 3,
               positionTop:'17',
               positionLeft:'11',
               title:'Aardappelen',
               text1:'De aardappel houdt verband met Bleijerheide in meerdere opzichten. Ten eerste is er het belang van de ondergrond voor het leven erboven. De schacht van de kolenmijn Neuprick daalde af in het hart van de huidige wijk en doorzeefde de aarde tot een diepte van 235 meter, bij lange na niet zo ingrijpend als de Domaniale maar evengoed van belang voor het bovengrondse leven. Er wordt gezegd dat de flats gebouwd zijn op een zeer beperkte fundering, dieper graven had geen zin want op een gatenkaas kun je immers niet bouwen.',
               text2:'Zoals een aardappel als onderaardse knol groeit om uiteindelijk gerooid te worden, tot voedsel te dienen en energie te leveren, zo diende de steenkool in de Bleijerheidse grond hetzelfde doel.  Echter, waar een aardappel in te passen is in een ecosysteem en er met behoedzame omgang met grondstoffen en energie een onuitputtelijke bodem kan blijven bestaan, zo is de kolenwinning per definitie een eindig leven beschoren. En zodoende bleven terreinen achter, verdwenen generaties en kwamen er nieuwe, om te beantwoorden aan de vraag naar arbeidskrachten op de tot industrie- en bedrijvenzones herbestemde mijnterreinen. Deze generatie zocht – en vond - huisvesting in en om de flats. De flats, die vanaf het begin waren als een vakkundig gemaakte mand vol vers gerooide aardappelen: van alle gemakken voorzien, veel licht, lucht en ruimte en met goede buren. En zoals een mand vol aardappelen een zekere mate van onderhoud nodig heeft, verdient ook de nieuwe micro-maatschappij in Bleijerheide een zekere toewijding. Want rotte knollen steken ook de rest aan: het tijdig verwijderen hiervan voorkomt besmetting. Dit heeft volgens velen het uiteindelijk lot van de flats bepaald: de rotte knollen zijn niet tijdig gesignaleerd, of sterker nog: zouden doelbewust in de mand zijn gelegd, al dan niet vanuit de ijdele hoop dat hiermee het rottingsproces een halt zou worden toegeroepen.',
            },
            {
               number: 7,
               positionTop:'47',
               positionLeft:'41',
               title:'Proportie',
               text1:'Een horizontale snede door de flat, die duidelijk te kennen geeft hoe eenduidig de toekomstwensen zijn: tien is te hoog. De helft zou mooi zijn. Of een laag of drie, vier. Zouden de flats gewoonweg gehalveerd kunnen worden? Dan renoveren we de onderste helft en blijft al het goede behouden. Op verschillende manieren is de proportie van de flats van belang. Feitelijk zijn ze meer breed dan hoog, maar het is met name de hoogte die in het oog springt. De doorwaadbaarheid van het gebied, tussen de flats door, is wonderbaarlijk goed: vaak zie je buurtbewoners tussen de flats wandelen, de hond uitlaten of van het groen genieten.',
               text2:'Af en toe wordt de klimaatverandering op microniveau verklaard aan de hand van het verschijnen of verdwijnen van de flats. Ze zouden als windscherm functioneren, de achterliggende woningen beschermen tegen de ijzige winterkoude, maar tegelijkertijd ook valwind veroorzaken. Ook de al dan niet humane wijze van huisvesten houdt verband met de hoogte. Het zou de vraag zijn of het stapelen van woningen tot wel tien lagen hoog überhaupt wel verantwoord is, vanuit menselijk en sociaal oogpunt bezien. Dit is vooral het perspectief van de omwonende: de (voormalige) flatbewoners zelf roemen vooral het uitzicht van bovenaf, de ruime appartementen en het feit dat je door de concentratie van woningen altijd wel iemand tegenkwam. Omwonenden van het eerste uur bleken overigens ook in staat te zijn van het uitzicht te genieten: de entrees en liften van de flats waren voor iedereen toegankelijk, hier werd dan ook verzameld om het vuurwerk in de omgeving goed te kunnen aanschouwen en naar hartenlust gespeeld door de kinderen van binnen én buiten.',
               text3:'De toegankelijkheid en hoogte bleken ook een schaduwzijde te hebben: voor wie een eind aan zijn leven wilde maken, vormden de flats een ideale springplank naar het hiernamaals. Het gerucht gaat dat er lijstjes bijgehouden werden vanaf welke flat in de wijde omtrek de meeste slachtoffers vielen. Tenslotte is er natuurlijk de zichtbaarheid. Niet alleen de tien verdiepingen, ook de ligging op een heuvel maakt dat de flats vanuit een groot gebied zichtbaar zijn. Steeds vanuit een andere hoek, maar steeds even herkenbaar door de uniformiteit van het ontwerp. Een oriëntatiepunt voor wie wil weten hoe ver – of dichtbij – hij of zij zich van zijn huis of bestemming bevindt.',
            },
            {
               number: 9,
               positionTop:'77',
               positionLeft:'38',
               title:'Gemeenschap',
               text1:'In de flat, tussen de haakjes, was er altijd wel iemand. Niet dat men iedereen kende, maar hoe laat (of vroeg)  het ook was, er was altijd wel een goedendag in de hal, in de lift of op de galerij. Ieder leefde wel zijn eigen leven, maar samen vormden ze een wereld op zich, een gemeenschap die terughoudend aanwezig was voor degene die het nodig had. Banden met de buitenwereld waren er ook meer dan voldoende, het dagelijks leven maakte geen onderscheid tussen flat- of huisbewoner. Gaandeweg is dit gevoel afgekalfd, de bewoners van weleer vertrokken en de nieuwelingen gedroegen zich alsof er geen ander bestond.',
            },
            {
               number: 10,
               positionTop:'85',
               positionLeft:'35',
               title:'Fundering',
               text1:'Dat de flat bouwtechnisch gezien zijn beste tijd achter de rug heeft, dat is geen geheim. De gestutte balkons zijn daar een duidelijk voorbeeld van, en het kraken en bewegen van de flats is ook meerdere malen ter sprake gekomen. Wilma, een aannemersbedrijf uit Weert dat bestond van 1939 tot 1998, heeft ze indertijd gebouwd, niet alleen hier, maar zowat in heel Nederland. Bij menigeen staan ze dan ook bekend als ‘de Wilma-flats’. Niet alleen de balkons schijnen een probleem te zijn, ook de fundering. Niemand weet exact hoe het zit maar er wordt gezegd dat deze bij lange na niet diep genoeg is, dat schijnt te maken te hebben met mijngangen en de daarmee samenvallende onmogelijkheid om in de diepere aardlagen steun te vinden. Of dit ook de voornaamste reden voor sloop is, dat lijkt nog bij niemand bekend. Waarschijnlijk zal het ook en vooral te maken hebben met een veranderende woonbehoefte en afschrijving.',
            },
            {
               number: 14,
               positionTop:'81',
               positionLeft:'18',
               title:'Rolluiken',
               text1:'In Limburg zie je ze regelmatig, in tegenstelling tot in de rest van Nederland. ’s Avonds, wanneer het donker is en mensen thuiskomen van hun werk, wordt het straatbeeld niet zelden bepaald door eentonige rijen woningen met hermetisch gesloten ramen en deuren. De bewoners mogen dan wel vriendelijke mensen zijn, maar zo’n straatbeeld wekt weinig sympathie en vertrouwen. Dit staat in schril contrast met de flat: hier was altijd wel een teken van leven, al was het maar door een enkel lampje dat nog ergens brandde.',
            },
            {
               number: 17,
               positionTop:'82',
               positionLeft:'60',
               title:'Voetballen',
               text1:'Er werd hier al gevoetbald voordat de flats er stonden. BVV Sparta, opgericht in 1914, speelde hier zijn wedstrijden en ligt hiermee als één van de fusieclubs waaruit Roda JC is ontstaan aan de basis van deze club. Sinds de bouw ligt het officiële voetbalveld van de wijk, bespeeld door KVC Oranje, ernaast maar werden de wedstrijden die er écht toe doen tussen de flats gespeeld. Jongeren uit de flats en uit de buurt samen, zonder enig onderscheid te maken in geboorteplaats, woonplaats of land van herkomst. Het bankje bij het voetbalveld staat in het geheugen van vele (inmiddels) volwassenen gegrift, op deze plek kwam je altijd wel iemand tegen, was het niet om daadwerkelijk tegen een bal te trappen, dan toch om een beetje rond te hangen. Ouders vonden het wel prima dat daar gespeeld werd want het was een veilig, overzichtelijk en goed omheind terrein.',
            },
            {
               number: 20,
               positionTop:'25',
               positionLeft:'69',
               title:'Teloorgang',
               text1:'Vroeger was het beter. Een armoedige dooddoener of zou er in het geval van Bleijerheide een kern van waarheid in schuilen? De bewoners van het eerste uur waren vooral locals, uit de wijk of van iets verder, maar bekend met – en in – de buurt. Later, zo vanaf de jaren ’80, veranderden geleidelijk de bewoners en daarmee het imago van de flats: niet meer de gedroomde ruime en moderne woningen voor jonge gezinnen maar de laatste woonmogelijkheid voor wie nergens anders terecht kon. Wanneer men probeert de situatie van de laatste jaren te beschrijven, voordat de leegstand zijn intrede deed, wordt er wederom gezegd dat er van alles in gestopt werd. Per flat verschilde de situatie nog een beetje. De D-flat aan de Ursulastraat was de meest nette, hier woonden bewoners van het eerste uur die niet alleen binnen de flat goed onderling contact hadden, maar ook met omwonenden. Zij bleven ook het langst, waardoor het beeld van deze flat geleidelijk transformeerde naar vijftig-plus-voorziening. Openlijk uitgevochten ruzies, met de jaren luider wordende muziek op de balkons, meer en meer afval dat naar beneden kwam via galerij en balkons (er is wel eens een televisie al dan niet bewust naar beneden gekieperd) en als laatste de leegstand: na meer dan 50 jaar is men niet de flats, maar wel de ‘zachte context’ ervan liever kwijt dan rijk.',
            },
            {
               number: 30,
               positionTop:'29',
               positionLeft:'5',
               title:'Papier prikken',
               text1:'Vroeger had iedere flat een eigen huismeester, dit was dan vaak een man uit één van de gezinnen die de verantwoordelijkheid nam of kreeg voor de flat en de ruimte eromheen. Nu is dat anders: vanuit HEEMwonen is er een huismeester benoemd voor het hele terrein. Het is geweldig hoe die man het nog in de gaten houdt. Hij loopt bijvoorbeeld regelmatig met een papierprikker om de flats heen en ruimt al het zwerfvuil op. Ook is hij nooit te beroerd bewoners aan te spreken op zwerfvuil of andere overtredingen. Hij ziet in zijn eentje toe op de omgeving en dat is goud waard, zeker sinds de leegstand.',
            },
            {
               number: 34,
               positionTop:'84',
               positionLeft:'53',
               title:'Zelfdoding',
               text1:'De toegankelijkheid en hoogte van de flats bleken al snel een schaduwzijde te hebben: voor wie een eind aan zijn leven wilde maken, vormden de flats een ideale springplank naar het hiernamaals. Het gerucht gaat dat er lijstjes bijgehouden werden vanaf welke flat in de wijde omtrek de meeste slachtoffers vielen, de laatste van slechts een paar maanden geleden, en wel hier: in Bleijerheide.',
            },
            {
               number: 36,
               positionTop:'21',
               positionLeft:'41',
               title:'Kraken',
               text1:'De flat ademt, letterlijk. Bij temperatuurverschillen hoor je ‘m af en toe kraken, knallen bijna. Het klinkt niet angstaanjagend, eerder vertrouwd. Bewegen deed hij sowieso al, waarschijnlijk om die reden zijn er een tijd geleden muurtjes gemetseld in de kelders, om de stijfheid te verhogen. Het geluid is er in ieder geval niet minder om geworden. Een levende reus die waakt over de wijk.',
            },
         ]
      }, 
       { 
         ref: '3-1', 
         src: 'sitefiles/images/map/map3-1.jpg',
         markers: [
            {
               number: 6,
               positionTop:'43',
               positionLeft:'25',
               title:'Ogen op straat',
               text1:'Vier maal honderd paar ogen. Ogen die signaleerden, bijvoorbeeld die keer dat het bezoek een handtas achterliet na het op slot zetten van de fiets en dit binnen enkele minuten via een telefoontje gemeld werd. Of de emmer die naast de auto bleef staan na het wassen, en waarop de autowasser binnen de kortste keren gewezen werd. Maar ook andersom: een oudere mevrouw die om hulp riep vanaf haar balkon en binnen de kortste keren de gevraagde hulp aangeboden kreeg. Jane Jacobs,  Amerikaans-Canadese publiciste en stadsactiviste, vooral bekend geworden door haar boek ‘The Death and Life of Great American Cities’, noemde dit verschijnsel ‘ogen op straat’. Ogen waarvan men wist dat er altijd wel een paar aanwezig was, die in de meeste gevallen een natuurlijk vertrouwd gevoel gaven en soms een gevoel van beklemming opriepen, aangezien ze deden beseffen dat hier niets ongezien kan gebeuren. Deze ogen - eerst Bleijerheids, later Kerkraads, Canadees, Amerikaans, Hollands, Bosnisch en uit landen waar de bewoners van het eerste uur het bestaan nauwelijks afwisten – deze ogen doofden langzaam uit. Allereerst richtten ze zich meer inwaarts; een gevolg van maatschappelijke en sociale veranderingen, zegt men. Daarna was er de onherroepelijke leegloop, als gevolg van de grote transformatie-opgave. Vanaf dit moment verloren de flats hun betekenis en meerwaarde en gingen ze meer vragen van de buurt dan ze konden geven.',
            },
            {
               number: 16,
               positionTop:'38',
               positionLeft:'12',
               title:'Bomen',
               text1:'Het groen rondom de flats wordt unaniem gewaardeerd. Het biedt een buffer tussen de flats en de tegenover liggende huizen, ooit aangenaam wegens verbale overlast, naar beneden gegooid afval en inkijk, tegenwoordig omdat het er nu eenmaal fraaier uitziet dan de resterende lege kolossen. Een enkeling ziet de bomen het liefst verdwijnen, de overlast van bladeren in de herfst waar de gemeente niet alert genoeg op is, weegt dan zwaarder dan de flat, die zonder bomen toch een stuk meer aanwezig zou zijn. De buren van deze enkeling denken hier dan weer anders over: de uilen en eekhoorns die hier nestelen zouden node gemist worden. En de schaduw die de bomen bieden, op het gras tussen de flats, wordt door veel hondenbezitters benut tijdens het uitlaten of spelen. Het schijnt overigens dat aan de drie bomen bij de kruising Jonkerbergstraat/Ursulastraat goed af te lezen is dat de Jonkerberstraat vroeger, voordat de flats gebouwd waren, evengoed een belangrijke straat was: ze markeren de kruising en tonen ook de exacte richting van de Jonkerbergstraat.',
            },
            {
               number: 18,
               positionTop:'9',
               positionLeft:'75',
               title:'Jeu de boules',
               text1:'Wat het voetbalveldje voor de jongeren betekende, geldt in iets mindere mate ook voor de jeu de boules-baan, in dit geval voor de ouderen. Ook hier was vaak wel aanspraak te vinden, en is menig zomeravond in gezelschap doorgebracht.',
            },
            {
               number: 21,
               positionTop:'26',
               positionLeft:'46',
               title:'Politie',
               text1:'Vroeger zag je ze hier eigenlijk nooit, dat was niet nodig. De laatste jaren was er regelmatig ruzie, en ook de drugsoverlast leek alsmaar meer in het oog te springen. Niet alleen sinds de leegstand, ook daarvoor al. Het was ooit heel erg, er lagen overal spuiten. Die zijn toen opgeruimd door de gemeente, maar die komt hier eigenlijk te weinig. Als je de dealers of junks betrapt en de politie belt, dan zijn ze altijd te laat natuurlijk. Het gaat ook zo vlug, een paar seconden en ze zijn alweer gevlogen. Echte overlast veroorzaakt het overigens niet, maar je voelt je toch minder veilig.',
            },
            {
               number: 22,
               positionTop:'43',
               positionLeft:'62',
               title:'De Bus',
               text1:'Elke dag reed er een bus, speciaal voor de Amerikanen en Canadezen van de AFCENT. Ze hadden weinig contact met de buurt, maar gedroegen zich steeds keurig. Heel soms kwamen ze wel eens in een winkel, het personeel deed dan zijn best om Engels te spreken. De bus heeft jaren gereden en hoorde eigenlijk bij de flats zoals de bewoners zelf.',
            },
            {
               number: 23,
               positionTop:'30',
               positionLeft:'77',
               title:'Konijntjes',
               text1:'Niet alleen vogels en eekhoorns, ook konijnen schijnen zich thuis te voelen tussen en rondom de flats. De natuurlijke glooiing van het terrein maakt de associatie met Waterschapsheuvel voor de hand liggend, een plek waar dieren leven, kinderen spelen en het leven goed is. Een idylle, in schril contrast met de huidige toestand van de flats. Ondanks dat wordt er nog veel gebruik gemaakt van het groen en is slechts de kans een konijntje tegen te komen al een meerwaarde voor de hele buurt. Het laatste gerucht dat de ronde doet is de aanwezigheid van vleermuizen in de flats: de sloop- en (nieuw)bouwplannen, die nog vrij onduidelijk zijn, lijken hiermee verder weg dan ooit.',
            },
            {
               number: 24,
               positionTop:'14',
               positionLeft:'86',
               title:'Standbeeld',
               text1:'De voorgeschiedenis van het beeld, dat er nu een beetje verscholen bij staat en enigszins vergeten lijkt, is waarschijnlijk nog maar bij een enkeling bekend. Op aandringen van een schooljuf schreven enkele leerlingen een brief aan de gemeente met de vraag om een standbeeld, meer specifiek een moeder met kind. De gemeente beantwoordde de vraag, maar het zou iets anders moeten worden dan een moeder met kind: deze hadden ze al genoeg. Hier in Bleijerheide zou iets meer hedendaags passen. Het beeld is daarna onder grote belangstelling onthuld door de kinderen die de brief schreven, of toch hiertoe aangezet werden door hun juf.',
            },
            {
               number: 25,
               positionTop:'71',
               positionLeft:'76',
               title:'Oude huisjes',
               text1:'Vroeger, voordat de flats gebouwd werden, stonden er huisjes op dit terrein. Niet veel hoor, een paar. Eigenlijk zoals heel dit gebied dun bebouwd was. Deze zijn gesloopt toen de flats gebouwd werden, maar ook tegen de vlakte gegaan voor het verzorgingstehuis. Sowieso werden er heel wat oude woningen afgebroken in die tijd, vanuit deze huisjes verhuisden mensen naar de flats. De eerste bewoners waren dus vooral mensen uit de wijk en nabije omgeving, niet zelden jonge stelletjes die hier voor het eerst samen gingen wonen. Er werd overigens overal in Bleijerheide gebouwd in die tijd, en aangezien er veel nieuwe inwoners bij kwamen was het niet duidelijk wie van de flats was en wie van elders. Nu zijn het vooral Duitsers die de oudere woningen opkopen, je hoort dan ook veel Duits op straat. Gekscherend wordt er dan ook wel eens gevraagd of je ook een Duits paspoort hebt wanneer je vertelt dat je in Bleijerheide woont.',
            },
            {
               number: 31,
               positionTop:'31',
               positionLeft:'82',
               title:'Hondenpoep',
               text1:'Een titel voor een thema dat geen thema is. Althans, als je het in de strikte zin van het woord beschouwt. Toch is het de moeite waard het onder de loep te nemen, figuurlijk uiteraard, omdat het iets kan zeggen over de algehele teloorgang van het terrein. Waar een speeltuin was, is nu een overwoekerd veldje, het voetbalveld ligt er verlaten bij, het standbeeld is nauwelijks meer zichtbaar en de vuilnisbakken zijn niet zelden overvol. Misschien dat het gebrek aan sociale controle maakt dat het terrein langzaam een achterkant wordt, waar het opruimen van hondenpoep niet noodzakelijk geacht wordt. In ieder geval lijkt het een indicator van een veranderende atmosfeer te zijn.',
            },
            {
               number: 39,
               positionTop:'72',
               positionLeft:'64',
               title:'Mensenmassa’s',
               text1:'Opvallend genoeg lijken mensen niet of nauwelijks te merken dat er in Bleijerheide zo’n 400 huishoudens minder wonen. Op straat is het niet minder druk, de middenstand heeft er wel mee te kampen maar ook hier is het geen significant verschil of een urgent thema. Vaak werd er gezegd dat de frituur er vast wel last van zal hebben, maar hier is pas een nieuwe eigenaar ingetrokken en deze lijkt aan belangstelling en klanten vooralsnog geen gebrek te hebben. Het zorgcentrum Vroenhof doet af en toe een bestelling, dat zal zeker ook meehelpen.',
            },
            {
               number: 40,
               positionTop:'82',
               positionLeft:'18',
               title:'De kerkklok, en verder',
               text1:'Over het uitzicht vanaf de flats raakt men niet uitgesproken: hoe hoger het appartement zich bevond, hoe meer waarde het om die reden leek te vertegenwoordigen. Niet alleen om de directe omgeving te bekijken, of verder weg, richting Aken, Monschau of zelfs naar België, maar ook om te weten hoe laat het was: voor wie in de flat woonde, was het niet zelden de kerkklok waarnaar in een reflex gekeken werd. En éénmaal per jaar was er een heuse samenkomst op de galerijen om het vuurwerk van de verre omgeving te aanschouwen. De galerijen waren dan ook voor iedereen toegankelijk in de beginjaren, dat maakte dat veel omwonenden wel eens een kijkje gingen nemen.',
            },
            {
               number: 44,
               positionTop:'26',
               positionLeft:'40',
               title:'Drugs',
               text1:'Het was er altijd wel, de Engelse flat had er mogelijk iets mee te maken: dat waren mensen die de wereld kenden en tijdens feestjes misschien wel eens wat gebruikten, al wist niemand dat zeker en had ook niemand er last van. Sinds ze van alles in de flats stoppen en niet meer zo selectief zijn qua bewoners is het wel veel erger geworden. Het heeft eigenlijk niets met de leegstand te maken, al zal het wel schelen dat er nu minder sociale controle is. Als je ze ziet dealen dan kun je wel de politie bellen maar die komen er al niet meer voor, en als ze al de moeite nemen dan komen ze toch te laat, die transacties zijn dan ook razendsnel afgehandeld. Het is misschien het lot van een grensgebied zoals dit.',
            },
         ]
      },
       { 
         ref: '3-2', 
         src: 'sitefiles/images/map/map3-2.jpg',
         markers: [
            {
               number: 19,
               positionTop:'67',
               positionLeft:'71',
               title:'740 meter diep',
               text1:'De mijn is niet zo sterk aanwezig in Bleijerheide, althans, als je het vergelijkt met de omliggende gebieden. In Chevremont bijvoorbeeld, daar waren veel meer koloniën. Je merkt dat nog, zegt men, het volk is daar anders. Wel schijnen de tot wel 740 meter diep reikende mijngangen ervoor gezorgd te hebben dat de aarde onder de flats een gatenkaas geworden is, met alle mogelijke problemen van dien.',
            },
            {
               number: 20,
               positionTop:'81',
               positionLeft:'50',
               title:'Teloorgang',
               text1:'Vroeger was het beter. Een armoedige dooddoener of zou er in het geval van Bleijerheide een kern van waarheid in schuilen? De bewoners van het eerste uur waren vooral locals, uit de wijk of van iets verder, maar bekend met – en in – de buurt. Later, zo vanaf de jaren ’80, veranderden geleidelijk de bewoners en daarmee het imago van de flats: niet meer de gedroomde ruime en moderne woningen voor jonge gezinnen maar de laatste woonmogelijkheid voor wie nergens anders terecht kon. Wanneer men probeert de situatie van de laatste jaren te beschrijven, voordat de leegstand zijn intrede deed, wordt er wederom gezegd dat er van alles in gestopt werd. Per flat verschilde de situatie nog een beetje. De D-flat aan de Ursulastraat was de meest nette, hier woonden bewoners van het eerste uur die niet alleen binnen de flat goed onderling contact hadden, maar ook met omwonenden. Zij bleven ook het langst, waardoor het beeld van deze flat geleidelijk transformeerde naar vijftig-plus-voorziening. Openlijk uitgevochten ruzies, met de jaren luider wordende muziek op de balkons, meer en meer afval dat naar beneden kwam via galerij en balkons (er is wel eens een televisie al dan niet bewust naar beneden gekieperd) en als laatste de leegstand: na meer dan 50 jaar is men niet de flats, maar wel de ‘zachte context’ ervan liever kwijt dan rijk.',
            },
            {
               number: 28,
               positionTop:'11',
               positionLeft:'80',
               title:'Zwembad',
               text1:'Op warme zomerdagen zette regelmatig iemand van de flat een zwembadje buiten, dit kon dan gebruikt worden door alle kinderen van de buurt. Zo droeg het initiatief van één gezin bij aan de speelvreugde van vele kinderen. Er was altijd wel een familie bereid hier tijd voor te maken en de verantwoordelijkheid te nemen, waardoor anderen hun steentje bijdroegen en het vullen met -of verversen van- het water op zich namen.',
            },
            {
               number: 29,
               positionTop:'15',
               positionLeft:'39',
               title:'Grasveld',
               text1:'Naast de daarvoor bedoelde speelplekken was er natuurlijk nog het gras naast en tussen de flats. Hier werd door zowel flat- als buurtbewoners dankbaar gebruik van gemaakt, door kinderen om er te spelen of rond te hangen en door volwassenen voor een ommetje, een rondje met de hond of voor een babbeltje in de zon of schaduw.',
            },
            {
               number: 30,
               positionTop:'13',
               positionLeft:'14',
               title:'Papier prikken',
               text1:'Vroeger had iedere flat een eigen huismeester, dit was dan vaak een man uit één van de gezinnen die de verantwoordelijkheid nam of kreeg voor de flat en de ruimte eromheen. Nu is dat anders: vanuit HEEMwonen is er een huismeester benoemd voor het hele terrein. Het is geweldig hoe die man het nog in de gaten houdt. Hij loopt bijvoorbeeld regelmatig met een papierprikker om de flats heen en ruimt al het zwerfvuil op. Ook is hij nooit te beroerd bewoners aan te spreken op zwerfvuil of andere overtredingen. Hij ziet in zijn eentje toe op de omgeving en dat is goud waard, zeker sinds de leegstand.',
            },
            {
               number: 38,
               positionTop:'21',
               positionLeft:'25',
               title:'Reinigingsdienst',
               text1:'De gemeente komt hier niet vaak genoeg, zegt men. Het is dat de huismeester nog dagelijks zijn ronde maakt om het vuil op te ruimen, maar het ligt hier vol met bladeren bijvoorbeeld. In de herfst wordt dit nat en glad, niet veilig en erg rommelig. De ruimte tussen de flats wordt dan wel weer schoon gehouden, terwijl daar niemand meer woont. Dat is toch absurd? Er is duidelijk minder aandacht voor dit stukje Kerkrade, ze vegen hier toch echt een stuk minder dan op de Markt bijvoorbeeld.',
            },
            {
               number: 43,
               positionTop:'29',
               positionLeft:'55',
               title:'Woonwagen',
               text1:'Het ziet er een beetje vreemd uit, dat hapje uit het terrein waar één woonwagen staat. Het is er altijd al geweest, bijna even lang als de flats er zijn. De huidige bewoonster woont hier al zo’n 40 jaar en heeft in al die jaren vele buren zien komen en gaan. De omgeving is veranderd, met andere mensen kwamen andere talen, andere gewoontes en daarna niets meer. De woningen tegenover haar zijn erbij gekomen, daar wonen nu jonge gezinnen en ook Duitsers. Maar dit taartpuntje lijkt al 40 jaar de enige constante in een steeds wisselende omgeving, en zou zomaar kunnen verdwijnen in de gewenste herinrichting van het terrein.',
            },
         ]
      },
       { 
         ref: '3-3', 
         src: 'sitefiles/images/map/map3-3.jpg',
         markers: [
            {
               number: 26,
               positionTop:'39',
               positionLeft:'74',
               title:'Afspraak op de Markt',
               text1:'Tekenend voor de hechte band tussen een deel van de bewoners van de D-flat is het feit dat ze, sinds de gedwongen verhuizingen naar elders, elkaar wekelijks treffen op de Kerkraadse Markt. Het keuvelen aan de jeu de boules-baan is verplaatst naar een even informeel maar minder regelmatig samenzijn. Met het uitwaaieren van de bewoners is de band gebleven, al zijn het vooral de volhouders en ‘nostalgisten’ die hun gevoel voor de flat levend houden. Overigens zijn het niet alleen de flatbewoners die elkaar blijven treffen: ook omwonenden hebben soms nog contact met voormalige flatbewoners die nu elders wonen.',
            },
            {
               number: 27,
               positionTop:'31',
               positionLeft:'45',
               title:'Kippenhok',
               text1:'Naast vooral positieve verwijzingen naar de saamhorigheid en gemeenschapszin binnen de flats, vooral in de beginjaren, klinken er ook negatieve geluiden.  Er wordt gesteld dat er nogal wat ‘aangerommeld’ werd in de flat, dat thuis blijvende vrouwen er nogal wat hobby’s en minnaars op zouden nahouden waar manlief beter niet van op de hoogte was. Dat zou niet alleen voor de flat gelden, maar het was daar wel erg opvallend aanwezig. Zo dicht op elkaar wonen zou daar vanzelf voor zorgen: zo’n groot aantal mensen op zo’n klein stukje is namelijk vragen om problemen en niet meer van deze tijd.',
            },
            {
               number: 35,
               positionTop:'53',
               positionLeft:'36',
               title:'Eigendomshuizen',
               text1:'Bleijerheide schijnt relatief veel eigendomswoningen te hebben. In Chevremont bijvoorbeeld ligt de verhouding huur/koop toch een stuk anders, dit maakt het een heel andere wijk. Dit zou vooral te maken hebben met het feit dat daar meer koloniën waren, waarvan de woningen vaak van de mijnen overgegaan zijn naar de woningcorporaties. Daarom zou het in Bleijerheide netter zijn, er woont ander volk.',
            },
            {
               number: 45,
               positionTop:'8',
               positionLeft:'33',
               title:'Speeltuin',
               text1:'Je had de jeu de boules-baan en het voetbalveld, maar het meest aantrekkelijke van de publieke ruimte rondom de flats was misschien toch wel de speeltuin. Een aantal toestellen met rubberen matten eromheen, vooral een ontmoetingsplek voor de kinderen. En wederom: niet alleen voor de kinderen uit de flat, maar ook (en misschien nog wel meer) voor de buurtkinderen. Net als het voetbalveld. Nu is die speeltuin weg en is het terrein een beetje overwoekerd.',
            },
         ]
      }, 
   ]
  
}]);