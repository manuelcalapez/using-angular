var app = angular.module("dearApp", []);

$( document ).ready(function() {
	$('.fade').slick({
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		autoplay: true,
		autoplaySpeed: 5000,
		pauseOnHover: false,
	});
	
	
	$('.navlink a').click(function(){
		var tccLinkIndex = $('.navlink a').index(this);
		
		var tccLink = $(this).attr('href');
		

		$('html, body').animate({
			scrollTop: $(tccLink).offset().top - 50}, 500);
		return false;

	});
	
	$('.quadrantOver').click(function(event){
		
		event.preventDefault();
		
		var thisLink = $('.quadrantOver').index(this);
		
		console.log(thisLink);
		
		//$('.mainMap').fadeOut(1000);
		
		
		$('.quadWrapper').fadeIn(1000);
		$('.quadrantSector').eq(thisLink).fadeIn(1000);
		
		setTimeout(function(){
			$('.mapNav').fadeIn(150);
		}, 1000);
		
		
		
		$('.quadrant').removeClass('active');
		$('.quadrant').eq(thisLink).addClass('active');
	
		
	});
	
	$('.quadrant').click(function(event){
		
		event.preventDefault();
		
		var thisLink = $('.quadrant').index(this);
		
		
		$('.quadrantSector').fadeOut(1000);
		$('.quadrantSector').eq(thisLink).fadeIn(1000);
		
		$('.quadrant').removeClass('active');
		$('.quadrant').eq(thisLink).addClass('active');
	
		
	});
	
	$('.backtomap').click(function(event){
		
		event.preventDefault();
		$('.mapNav').fadeOut(150);
		
		setTimeout(function(){
			$('.mainMap').fadeIn(1000);
		$('.quadrantSector').fadeOut(1000);
		$('.quadrant').removeClass('active');
			$('.quadWrapper').fadeOut(1000);
		}, 150);
		
	});
	
	$('.marker').mouseenter(function(){
		
		var thisLink = $('.marker').index(this);
		
		if(!($('.marker').eq(thisLink).hasClass('active'))){
			$('.marker').removeClass('.active');
			$('.markerInfo').fadeOut('medium');
			$('.marker').eq(thisLink).find('.markerInfo').fadeIn('slow');
			$('.marker').eq(thisLink).addClass('active');
		}
		
		
		
	});
	
	$('.close').click(function(){
		
		var thisLink = $('.marker').index(this);
		
		$('.marker').removeClass('active');
		$('.markerInfo').fadeOut('medium');
		
	});
	
	$('.markerInfo').mouseleave(function(){
		
		var thisLink = $('.marker').index(this);
		
		$('.marker').removeClass('active');
		$('.markerInfo').fadeOut('medium');
		
	});
	
	$('#fotos .print').click(function(){
		
		var pdf_url = 'sitefiles/pdf/fotos/foto' + $(this).attr('val') + '.pdf';
		
		console.log(pdf_url);
		
		var w = window.open(pdf_url);
		
	});
	
	$('#kaart .print').click(function(){
		
		var pdf_url = 'sitefiles/pdf/markers/marker' + $(this).attr('val') + '.pdf';
		
		console.log(pdf_url);
		
		var w = window.open(pdf_url);
		
	});
	
	$('.general .print').click(function(){
		
		var pdf_url = 'sitefiles/pdf/' + $(this).attr('val') + '.pdf';
		
		console.log(pdf_url);
		
		var w = window.open(pdf_url);
		
	});
	
	
	
});
