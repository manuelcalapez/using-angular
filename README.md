# README #

Using Angular to make a dynamic local website.

### What is this repository for? ###
A memory guide for using Angular. This is a real project developed in 2016.
index.html is the only html file and angular is used on the slideshow, map quadrant and map markers.

controller folder in js folder has all the JSON that genertes the above data.

As this was a local website that had to run with no internet connection all assets and needed files are in the project.
Because it was a local project image size had no limit and was preferred to be large and non-optimised in order to preserve detail.

### Owner ###
Developed by Manuel Calapez for Dear Hunter in 2016